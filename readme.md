# Oh My Food !

## Résumé

Cette application permet est déstinée au milieu de la restauration, il permet de faire le lien entre la cuisine et les commandes en salle.
Le but de l'application est de 
* pouvoir ajouter des menu à la carte côté cuisine avec un stock limité
* passer commande côté salle et les envoyer en cuisine
* récupérer les commandes côté cuisine pour les préparer

## Installation

## Stack technique

Spring Boot
MariaDB
Swing


## Auteur

Oualid Ismaili
Khaled Razouk
Ling Curty
Roland Foucher



