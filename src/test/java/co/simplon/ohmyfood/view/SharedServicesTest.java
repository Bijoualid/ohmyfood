package co.simplon.ohmyfood.view;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;
import co.simplon.ohmyfood.entity.Beverage;
import co.simplon.ohmyfood.entity.Dessert;
import co.simplon.ohmyfood.entity.Food;

public class SharedServicesTest {



  @Test
  void testConvertListToHashMap() {
    List<Food> foodList = new ArrayList<>();
    Beverage beverage = new Beverage("name", 12, 12);
    Beverage beverage2 = new Beverage("name", 12, 12);
    Dessert dessert = new Dessert("name", 12, 12);
    foodList.add(beverage);
    foodList.add(beverage);
    foodList.add(beverage2);
    foodList.add(dessert);
    foodList.add(dessert);
    foodList.add(dessert);

    Map<? extends Food, Long> convertListToHashMap = SharedServices.convertListToHashMap(foodList);
    assertEquals(2, convertListToHashMap.get(beverage));
    assertEquals(3, convertListToHashMap.get(dessert));

  }
}
