package co.simplon.ohmyfood.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import co.simplon.ohmyfood.entity.Beverage;
import co.simplon.ohmyfood.entity.Dessert;
import co.simplon.ohmyfood.entity.Dish;
import co.simplon.ohmyfood.entity.OrderTable;
import co.simplon.ohmyfood.entity.Waiter;
import co.simplon.ohmyfood.exception.NoFoodInOrderException;
import co.simplon.ohmyfood.exception.NoWaitersInDatabaseException;
import co.simplon.ohmyfood.exception.NotEnoughtStockOfFoodException;
import co.simplon.ohmyfood.repository.BeverageRepository;
import co.simplon.ohmyfood.repository.DessertRepository;
import co.simplon.ohmyfood.repository.DishRepository;
import co.simplon.ohmyfood.repository.OrderRepository;
import co.simplon.ohmyfood.repository.WaiterRepository;

@ExtendWith(MockitoExtension.class)
public class RestaurantControllerTest {

  // Mock repostitories
  @Mock
  BeverageRepository beverageRepository;

  @Mock
  DessertRepository dessertRepository;

  @Mock
  DishRepository dishRepository;

  @Mock
  OrderRepository orderRepository;

  @Mock
  WaiterRepository waiterRepository;

  @InjectMocks
  RestaurantController restaurantController;



  @Test
  void testGetBeverages() {
    List<Beverage> beverages = new ArrayList<>(List.of(new Beverage(), new Beverage()));
    when(beverageRepository.findAll()).thenReturn(beverages);
    assertEquals(2, restaurantController.getBeverages().size());
  }

  @Test
  void testGetDessert() {
    List<Dessert> desserts = new ArrayList<>(List.of(new Dessert(), new Dessert()));
    when(dessertRepository.findAll()).thenReturn(desserts);
    assertEquals(2, restaurantController.getDessert().size());
  }

  @Test
  void testGetDish() {
    List<Dish> dishes = new ArrayList<>(List.of(new Dish(), new Dish()));
    when(dishRepository.findAll()).thenReturn(dishes);
    assertEquals(2, restaurantController.getDish().size());
  }

  @Test
  void saveOrderTable_When_NoFood_Then_ThrowException() {
    OrderTable orderTable = new OrderTable(1, LocalTime.now());
    assertThrows(NoFoodInOrderException.class, () -> restaurantController.SaveOrder(orderTable));

  }

  @Test
  void saveOrderTable_When_FoodIsNotEnougth_Then_ThrowException() {
    OrderTable orderTable = new OrderTable(1, LocalTime.now());
    orderTable.getDishes().add(new Dish());
    assertThrows(NotEnoughtStockOfFoodException.class,
        () -> restaurantController.SaveOrder(orderTable));
  }

  @Test
  void findAllWaiters_When_NoWaitersInDatabase_Then_ThrowException() {
    List<Waiter> waiterListEmpty = new ArrayList<>();
    when(waiterRepository.findAll()).thenReturn(waiterListEmpty);
    assertThrows(NoWaitersInDatabaseException.class, () -> restaurantController.findAllWaiters());
  }

  @Test
  void findAllWaiters_Then_returnListSortedByName() throws NoWaitersInDatabaseException {
    List<Waiter> waiterList =
        new ArrayList<>(List.of(new Waiter("John", "John"), new Waiter("Alfred", "Dupond")));
    when(waiterRepository.findAll()).thenReturn(waiterList);
    assertEquals("Dupond", restaurantController.findAllWaiters().get(0).getLastName());

  }
}
