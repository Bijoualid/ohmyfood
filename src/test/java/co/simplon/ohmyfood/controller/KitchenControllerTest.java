package co.simplon.ohmyfood.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import co.simplon.ohmyfood.entity.OrderTable;
import co.simplon.ohmyfood.repository.OrderRepository;

@ExtendWith(MockitoExtension.class)
public class KitchenControllerTest {

  @Mock
  OrderRepository orderRepository;

  @InjectMocks
  KitchenController kitchenController;

  @Test
  void testGetOrders() {
    List<OrderTable> orders = new ArrayList<>(List.of(new OrderTable(2, 2, LocalTime.now()),
        new OrderTable(3, 2, LocalTime.now()), new OrderTable(1, 2, LocalTime.now())));
    when(orderRepository.findAll()).thenReturn(orders);
    assertEquals(2, orders.get(0).getId());
    assertEquals(1, kitchenController.getOrders().get(0).getId());
    assertEquals(2, kitchenController.getOrders().get(1).getId());

  }
}
