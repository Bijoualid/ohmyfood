package co.simplon.ohmyfood.repository;


import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.NoSuchElementException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import co.simplon.ohmyfood.entity.Waiter;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)

public class WaiterRepositoryTest {
  @Autowired
  WaiterRepository waiterRepository;

  @Test
  void findWaiterById() {
    assertNotNull(waiterRepository.findById(1).get());
  }

  @Test
  void getExceptionWhenIdNotExist() {
    assertThrows(NoSuchElementException.class, () -> waiterRepository.findById(10).get());
  }

  @Test
  void findAllWaiter() {
    assertNotEquals(0, waiterRepository.findAll().size());
  }

  @Test
  void saveWaiterToDatabase() {
    Waiter waiter = new Waiter("firstName", "lastName");
    assertNull(waiter.getId());
    waiterRepository.save(waiter);
    assertNotNull(waiter.getId());
  }


}

