package co.simplon.ohmyfood.repository;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import co.simplon.ohmyfood.entity.OrderTable;

// Pour information, les test DataJpaTest ne prennet pas en compte les modes fetchType de springBoot
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class OrderRepositoryTest {

  @Autowired
  OrderRepository orderRepository;



  @Test
  void testFindByIdOrder() {
    OrderTable order = orderRepository.findById(1).get();
    assertNotNull(order);
    assertNotNull(order.getWaiter());
    System.out.println(order.getBeverages().get(0).getName());
    assertNotEquals(0, order.getBeverages().size());
    assertNotEquals(0, order.getDesserts().size());
    assertNotEquals(0, order.getDishes().size());
  }

  @Test
  void testFindAllOrders() {
    List<OrderTable> orders = orderRepository.findAll();
    assertNotEquals(0, orders.size());
    assertNotEquals(0, orders.get(0).getBeverages().size());
    assertNotEquals(0, orders.get(0).getDesserts().size());
    assertNotEquals(0, orders.get(0).getDishes().size());
  }



}
