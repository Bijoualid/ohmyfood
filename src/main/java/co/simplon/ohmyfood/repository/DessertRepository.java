package co.simplon.ohmyfood.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.simplon.ohmyfood.entity.Dessert;

@Repository
public interface DessertRepository extends JpaRepository<Dessert, Integer> {
    
}
