package co.simplon.ohmyfood.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.simplon.ohmyfood.entity.Beverage;

@Repository
public interface BeverageRepository extends JpaRepository<Beverage, Integer> {

}
