package co.simplon.ohmyfood.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.simplon.ohmyfood.entity.Dish;

@Repository
public interface DishRepository extends JpaRepository<Dish, Integer> {
    
}
