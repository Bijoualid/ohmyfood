package co.simplon.ohmyfood.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import co.simplon.ohmyfood.entity.Waiter;

public interface WaiterRepository extends JpaRepository <Waiter,Integer>{
    
}
