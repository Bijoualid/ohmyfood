package co.simplon.ohmyfood.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import co.simplon.ohmyfood.entity.OrderTable;


@Repository
public interface OrderRepository extends JpaRepository<OrderTable, Integer> {

}
