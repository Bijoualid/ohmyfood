package co.simplon.ohmyfood.exception;

public class NoWaitersInDatabaseException extends Exception {

  @Override
  public String getMessage() {

    return "Pas de serveurs dans la base de donnée !";
  }

}
