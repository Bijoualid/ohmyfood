package co.simplon.ohmyfood.exception;

public class NotEnoughtStockOfFoodException extends Exception {

  @Override
  public String getMessage() {

    return "Plus d'article en stock";
  }



}
