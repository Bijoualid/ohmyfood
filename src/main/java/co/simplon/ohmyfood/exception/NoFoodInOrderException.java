package co.simplon.ohmyfood.exception;

public class NoFoodInOrderException extends Exception {

  @Override
  public String getMessage() {

    return "Aucun plats ou boissons séléctionnés !";
  }

}
