package co.simplon.ohmyfood.controller;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import co.simplon.ohmyfood.entity.Beverage;
import co.simplon.ohmyfood.entity.Dessert;
import co.simplon.ohmyfood.entity.Dish;
import co.simplon.ohmyfood.entity.Food;
import co.simplon.ohmyfood.entity.OrderTable;
import co.simplon.ohmyfood.entity.Waiter;
import co.simplon.ohmyfood.exception.NoFoodInOrderException;
import co.simplon.ohmyfood.exception.NoWaitersInDatabaseException;
import co.simplon.ohmyfood.exception.NotEnoughtStockOfFoodException;
import co.simplon.ohmyfood.repository.BeverageRepository;
import co.simplon.ohmyfood.repository.DessertRepository;
import co.simplon.ohmyfood.repository.DishRepository;
import co.simplon.ohmyfood.repository.OrderRepository;
import co.simplon.ohmyfood.repository.WaiterRepository;
import co.simplon.ohmyfood.view.SharedServices;

/**
 * Service controller for restaurant page
 * 
 */
@Component

public class RestaurantController {

  @Autowired
  private OrderRepository orderRepository;

  @Autowired
  private BeverageRepository beverageRepository;

  @Autowired
  private DessertRepository dessertRepository;

  @Autowired
  private DishRepository dishRepository;

  @Autowired
  private WaiterRepository waiterRepository;


  /**
   * Get beverage from database and convert beverage list to food list
   * 
   * @return list of food whith beverage children
   */
  public List<? extends Food> getBeverages() {

    return beverageRepository.findAll();
  }

  /**
   * Get dish from database and convert dish list to food list
   * 
   * @return list of food whith dish children
   */
  public List<? extends Food> getDish() {

    return dishRepository.findAll();
  }

  /**
   * Get dessert from database and convert dessert list to food list
   * 
   * @return list of food whith dessert children
   */
  public List<? extends Food> getDessert() {

    return dessertRepository.findAll();
  }

  /**
   * save order to database, check if there is minimum 1 food in OrderTable entity before call
   * orderRepository.save()
   * 
   * @param orderTable order to save in database
   * @return true if save OK
   * @throws NoFoodInOrderException if there is no food in orderTable
   * @throws NotEnoughtStockOfFoodException if there is not enougth food stock quantity
   */
  public boolean SaveOrder(OrderTable orderTable)
      throws NoFoodInOrderException, NotEnoughtStockOfFoodException {

    // check if there are foods

    if (orderTable.getBeverages().size() == 0 && orderTable.getDesserts().size() == 0
        && orderTable.getDishes().size() == 0) {
      throw new NoFoodInOrderException();
    }

    // check food stock
    checkFoodStock(orderTable.getBeverages());
    checkFoodStock(orderTable.getDesserts());
    checkFoodStock(orderTable.getDishes());


    if (orderRepository.save(orderTable) == orderTable) {
      // decrement food
      orderTable.getBeverages().forEach(this::decrementFood);
      orderTable.getDesserts().forEach(this::decrementFood);
      orderTable.getDishes().forEach(this::decrementFood);

      return true;
    }
    return false;

  }

  /**
   * check if stock in foodList is greater than 0
   * 
   * @param foodList foodList to check
   * @throws NotEnoughtStockOfFoodException if food have stock less than 1
   */
  public void checkFoodStock(List<? extends Food> foodList) throws NotEnoughtStockOfFoodException {
    Map<? extends Food, Long> foodMap = SharedServices.convertListToHashMap(foodList);

    for (Map.Entry<? extends Food, Long> set : foodMap.entrySet()) {
      if (set.getKey().getStock() < set.getValue()) {
        throw new NotEnoughtStockOfFoodException();
      }
    }

  }

  /**
   * find all waiters in database and sort them by alphabetical order An better way is to find all
   * with order by name query in repository but is a good exercice to use Collections.sort()
   * 
   * @return list of waiters
   * @throws NoWaitersInDatabaseException if waiter table is empty
   */
  public List<Waiter> findAllWaiters() throws NoWaitersInDatabaseException {
    List<Waiter> waiters = waiterRepository.findAll();
    if (waiters.size() == 0) {
      throw new NoWaitersInDatabaseException();
    } else {
      Collections.sort(waiters);
      return waiters;
    }
  }

  /**
   * decrement the stock of food and save to database. Check wath kind of children is food to choice
   * the good repository
   * 
   * @param food mother class
   * @return food to make unit test
   */
  public Food decrementFood(Food food) {

    if (food.getClass() == Dessert.class) {
      Dessert dessert = dessertRepository.findById(food.getId()).get();
      dessert.decreaseStock();
      return dessertRepository.save(dessert);
    }
    if (food.getClass() == Dish.class) {
      Dish dish = dishRepository.findById(food.getId()).get();
      dish.decreaseStock();
      return dishRepository.save(dish);
    }
    if (food.getClass() == Beverage.class) {
      Beverage beverage = beverageRepository.findById(food.getId()).get();
      beverage.decreaseStock();
      return beverageRepository.save(beverage);
    }
    return null;

  }
}
