package co.simplon.ohmyfood.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.simplon.ohmyfood.entity.OrderTable;

import co.simplon.ohmyfood.repository.OrderRepository;


@Component
public class KitchenController {

  @Autowired
  private OrderRepository orderRepository;


  /**
   * Find all orders sort them by ID
   * 
   * @return list of orders
   */
  public List<OrderTable> getOrders() {

    List<OrderTable> orders = orderRepository.findAll();

    return orders.stream().sorted((x, y) -> x.getId().compareTo(y.getId())).toList();

  }

  public void deleteOrder(OrderTable order) {

    orderRepository.delete(order);


  }



}
