package co.simplon.ohmyfood.view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class InitChoice extends JPanel {

  @Autowired
  @Lazy
  private Window window;

  /**
   * main page with init choice Restaurant or Kitchen Make two button : action = add good Jpanel to
   * main frame to display
   * 
   */
  public InitChoice() {

    setLayout(new GridBagLayout());
    add(restaurantButton(), btnContrain());
    add(kitchenButton(), btnContrain());

  }

  /**
   * Diffined the structure of gridBag
   * 
   * @return gridBagContrains
   */
  public GridBagConstraints btnContrain() {
    GridBagConstraints constraints = new GridBagConstraints();
    constraints.fill = GridBagConstraints.CENTER;
    constraints.ipadx = 80;
    constraints.ipady = 80;
    constraints.weightx = 1;
    return constraints;
  }

  /**
   * Make the restaurantButtun with listener
   * 
   * @return Jbutton restaurant
   */
  public JButton restaurantButton() {

    JButton restaurantButton = MakeButton("Restaurant", Color.BLUE);
    restaurantButton.addActionListener((e) -> window.displayRestaurantPage());
    return restaurantButton;
  }

  /**
   * Make the kitchen Button with listener
   * 
   * @return Jbutton kitchen
   */
  public JButton kitchenButton() {

    JButton kitchen = MakeButton("kitchen", Color.RED);
    kitchen.addActionListener((e) -> window.displayKitchenPage());
    return kitchen;
  }

  /**
   * method to make the button
   * 
   * @param name name display in button
   * @param color color of text button
   * @return created button
   */
  public JButton MakeButton(String name, Color color) {
    JButton button = new JButton(name);
    button.setForeground(color);
    return button;
  }

}
