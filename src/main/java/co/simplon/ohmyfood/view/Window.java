package co.simplon.ohmyfood.view;

import java.awt.EventQueue;
import javax.swing.JFrame;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import co.simplon.ohmyfood.view.kitchen.Kitchen;


/**
 * creation of the windows application with extend JFrame Implements CommandLineRunner to be run
 * with SpringBoot Add component application to integrate this class to Spring Boot
 */
@Component
public class Window extends JFrame implements CommandLineRunner {

  @Autowired
  private Restaurant restaurant;

  @Autowired
  private Kitchen kitchen;

  @Autowired
  private InitChoice initChoice;



  /**
   * methode will be run on SpringBoot start use invokeLater to display the window when all other
   * springBoot events are process
   */
  @Override
  public void run(String... arg0) {

    EventQueue.invokeLater(new Runnable() {

      public void run() {
        initFrame();
        setContentPane(initChoice);
      }
    });
  }

  /**
   * configure the window
   */
  public void initFrame() {

    setSize(1280, 720);
    setVisible(true);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

  }

  public void displayRestaurantPage() {
    setContentPane(restaurant);
    revalidate();
    repaint();
  }

  public void displayKitchenPage() {
    setContentPane(kitchen);
    revalidate();
    repaint();
  }

}
