package co.simplon.ohmyfood.view.restaurant;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import javax.annotation.PostConstruct;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import co.simplon.ohmyfood.entity.Food;
import co.simplon.ohmyfood.view.SharedServices;



@Component
public class RightSide extends JPanel {

  @Autowired
  private AllFoodRows allFoodRows;

  @Autowired
  private RightSideUp rightSideUp;

  @Autowired
  private SaveOrder saveOrder;

  private JPanel rightSide, centerPanel;
  private Border borderRed = BorderFactory.createLineBorder(Color.red);

  /**
   * rigth side of the Restaurant page, add all blocks JPanel to display this part
   * 
   * @return rightSide JPanel
   */
  @PostConstruct
  public void rigthSide() {

    // foodMap = new HashMap<>();
    setLayout(new BorderLayout());
    add(rightSideUp, BorderLayout.NORTH);
    add(rightSideCenter(), BorderLayout.CENTER);
    add(saveOrder, BorderLayout.SOUTH);
    refreshPage();

  }

  /**
   * Refresh the JPanel with date for display hour every seconds
   */
  public void refreshPage() {
    Timer timer = new Timer();
    timer.scheduleAtFixedRate(new TimerTask() {
      @Override
      public void run() {
        // refresh time

        rightSideUp.remove(rightSideUp.getDate());
        rightSideUp.date();
        rightSideUp.add(rightSideUp.getDate(), 0);

        // refresh foodlist
        remove(centerPanel);
        add(rightSideCenter());
        revalidate();
        repaint();
      }
    }, 1000, 1000);
  }

  /**
   * @return JLabel to make a group with price and list of food to display in preview
   */
  public JPanel rightSideCenter() {
    centerPanel = new JPanel(new BorderLayout());
    centerPanel.add(foodListOrderPreview(), BorderLayout.NORTH);
    centerPanel.add(displayTotalPrice(), BorderLayout.SOUTH);

    return centerPanel;
  }

  /**
   * methode to display list of orders selected in the restaurant page into the right panel
   * 
   * @return JPanel with list of orders
   */
  public JPanel foodListOrderPreview() {
    JPanel foodListOrder = new JPanel();
    foodListOrder.setLayout(new BoxLayout(foodListOrder, BoxLayout.Y_AXIS));

    addFoodToListOrderPreview(foodListOrder, allFoodRows.getDishRow());
    addFoodToListOrderPreview(foodListOrder, allFoodRows.getDessertRow());
    addFoodToListOrderPreview(foodListOrder, allFoodRows.getBerverageRow());

    return foodListOrder;

  }

  /**
   * methode to loop in foodList for add a new JPanel with foodName in foodListOrder
   * 
   * @param foodListOrder Panel that display food selected in restaurant page
   * @param foodRow foodList selected will be saved in database
   */
  public void addFoodToListOrderPreview(JPanel foodListOrder, OneFoodRow onefoodRow) {

    // convert list to arraylist with recurrency in value
    Map<? extends Food, Long> foodMapToPreview =
        SharedServices.convertListToHashMap(onefoodRow.getFoodToOrder());

    foodMapToPreview.forEach((food, quantity) -> {

      JPanel foodPanel = new JPanel(new BorderLayout());
      JLabel foodLabel = new JLabel(food.getName() + " x" + quantity);


      // Button increase
      JButton increaseThisFood = new JButton("+");
      increaseThisFood.addActionListener((e) -> increaseButtonAction(food));

      // button decrease
      JButton decreaseThisFood = new JButton("-");
      decreaseThisFood.addActionListener((e) -> decreaseButtonAction(food));

      // add buttons in JPanel
      foodLabel.setHorizontalAlignment(JLabel.CENTER);
      foodPanel.add(foodLabel, BorderLayout.CENTER);
      foodPanel.add(increaseThisFood, BorderLayout.EAST);
      foodPanel.add(decreaseThisFood, BorderLayout.WEST);
      foodPanel.setBorder(borderRed);

      foodListOrder.add(foodPanel);
    });
  }

  /**
   * increase food in foodList on click
   */
  public void increaseButtonAction(Food food) {
    increaseFoodListToOrder(allFoodRows.getDishRow(), food);
    increaseFoodListToOrder(allFoodRows.getDessertRow(), food);
    increaseFoodListToOrder(allFoodRows.getBerverageRow(), food);
  }

  /**
   * decrease food in foodList on click
   */
  public void decreaseButtonAction(Food food) {
    decreaseFoodListToOrder(allFoodRows.getDishRow(), food);
    decreaseFoodListToOrder(allFoodRows.getDessertRow(), food);
    decreaseFoodListToOrder(allFoodRows.getBerverageRow(), food);
  }

  /**
   * delete food in foodRow list
   * 
   * @param foodRow foodrow to delete food
   * @param food food to delete in list
   */
  public void decreaseFoodListToOrder(OneFoodRow foodRow, Food food) {
    if (foodRow.getFoodToOrder().contains(food)) {
      foodRow.getFoodToOrder().remove(food);
    }
  }

  /**
   * add food in foodRow list
   * 
   * @param foodRow foodrow to add food
   * @param food food to add in list
   */
  public void increaseFoodListToOrder(OneFoodRow foodRow, Food food) {
    if (foodRow.getFoodToOrder().contains(food)) {
      foodRow.getFoodToOrder().add(food);
    }
  }

  /**
   * Methode to add total price in a JPanel
   * 
   * @return JPanel with totalPrice
   */
  public JPanel displayTotalPrice() {
    JPanel totalPricePanel = new JPanel();
    JLabel totalPricLabel = new JLabel(" Total : " + totalPrice() + " €");
    totalPricePanel.add(totalPricLabel);
    totalPricePanel.setBorder(borderRed);
    return totalPricePanel;
  }

  /**
   * Methode to calculate the totalPrice of a foodList
   * 
   * @param foodList foodlist to calculate totalPrice
   * @return totalPrice of One foodList
   */
  public int loopOnOrderPrice(List<? extends Food> foodList) {

    return foodList.stream().map(x -> x.getPrice()).reduce(0, Integer::sum);
  }

  /**
   * Calculate totalPrice of all FoodList to make a total Preview
   * 
   * @return totalPrice of all foodList
   */
  public int totalPrice() {
    return loopOnOrderPrice(allFoodRows.getDishRow().getFoodToOrder())
        + loopOnOrderPrice(allFoodRows.getDessertRow().getFoodToOrder())
        + loopOnOrderPrice(allFoodRows.getBerverageRow().getFoodToOrder());
  }



  //
  // getters
  //


  public JPanel getRightSide() {
    return rightSide;
  }

  public JPanel getCenterPanel() {
    return centerPanel;
  }

}

