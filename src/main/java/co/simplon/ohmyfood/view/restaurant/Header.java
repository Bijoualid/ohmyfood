package co.simplon.ohmyfood.view.restaurant;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import org.springframework.stereotype.Component;

/**
 * Header of app, must be re-used in other pages
 */
@Component
public class Header extends JPanel {

  /**
   * Constructor to make the header with his two parts
   */
  public Header() {

    setLayout(new BorderLayout());
    setBackground(Color.ORANGE);
    setPreferredSize(new Dimension(100, 100));
    setVisible(true);

    add(leftHeader(), BorderLayout.WEST);
    add(rigthText(), BorderLayout.EAST);
  }

  /**
   * Text comment in the right side of the Header
   * 
   * @return Jlabel with right side of the header
   */
  public JLabel rigthText() {
    JLabel rigthText = new JLabel("Solution de prise de commandes");
    rigthText.setPreferredSize(new Dimension(400, 100));
    rigthText.setHorizontalAlignment(SwingConstants.CENTER);
    return rigthText;
  }

  /**
   * Header Title in the leftSide with logo
   * 
   * @return JLabel with title and logo
   */
  public JPanel leftHeader() {

    JPanel leftHeader = new JPanel();
    leftHeader.setBackground(Color.ORANGE);

    JLabel leftText = new JLabel("OhMyFood !");

    leftText.setPreferredSize(new Dimension(200, 100));
    leftText.setHorizontalAlignment(SwingConstants.CENTER);

    leftHeader.add(leftText, BorderLayout.WEST);
    leftHeader.add(logo(), BorderLayout.EAST);

    return leftHeader;
  }

  /**
   * Methode to add file img logo to a JLabel
   * 
   * @return Jlabel with logo
   */
  public JLabel logo() {
    BufferedImage imageFile;

    try {
      imageFile = ImageIO.read(new File("assets/unnamed.png"));
      Image imageScaled = imageFile.getScaledInstance(100, 63, Image.SCALE_SMOOTH);

      JLabel label = new JLabel(new ImageIcon(imageScaled));

      return label;
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;

  }
}
