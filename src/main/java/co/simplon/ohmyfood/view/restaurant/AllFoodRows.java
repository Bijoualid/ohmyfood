package co.simplon.ohmyfood.view.restaurant;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Timer;
import java.util.TimerTask;
import javax.annotation.PostConstruct;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import co.simplon.ohmyfood.controller.RestaurantController;

@Component
public class AllFoodRows extends JPanel {

  // entities attributs
  private OneFoodRow dishRow, dessertRow, beverageRow;
  private GridBagConstraints constraints;

  @Autowired
  private RestaurantController restaurantController;


  @PostConstruct
  public void init() {
    dishRow = new OneFoodRow(restaurantController.getDish());
    dessertRow = new OneFoodRow(restaurantController.getDessert());
    beverageRow = new OneFoodRow(restaurantController.getBeverages());

    displayAllFoodList();
    refreshPage();
  }

  /**
   * Refresh all the foods rows every seconds
   */
  public void refreshPage() {
    Timer timer = new Timer();
    timer.scheduleAtFixedRate(new TimerTask() {
      @Override
      public void run() {
        // remove all lines
        removeAll();

        // get foods from database
        dishRow.setDatabaseFoodList(restaurantController.getDish());
        dessertRow.setDatabaseFoodList(restaurantController.getDessert());
        beverageRow.setDatabaseFoodList(restaurantController.getBeverages());

        // recreate buttons
        dishRow.loopOnFood();
        dessertRow.loopOnFood();
        beverageRow.loopOnFood();

        // display rows on AllFoodRows JPanel
        displayAllFoodList();
        repaint();
        revalidate();

      }
    }, 1000, 1000);
  }

  /**
   * Display all rows in restaurant page in gridLayout Jpanel
   * 
   * @param restaurantController controller autowired in main window
   * @return Jpanel with all foods row
   */

  public void displayAllFoodList() {

    // setLayout(new GridLayout(6, 1));

    setLayout(new GridBagLayout());
    constraints = constraintsFoodList();


    addLineWithContraints(0, "Plats");
    addLineWithContraints(100, dishRow);

    addLineWithContraints(0, "Desserts");
    addLineWithContraints(100, dessertRow);

    addLineWithContraints(0, "Boissons");
    addLineWithContraints(100, beverageRow);


  }

  public void addLineWithContraints(int padding, String name) {
    constraints.ipady = padding;
    add(titleFood(name), constraints);
  }

  public void addLineWithContraints(int padding, OneFoodRow foodRow) {
    constraints.ipady = padding;
    add(foodRow, constraints);
  }

  /**
   * gridBagContraints configuration for all foodList block to display
   * 
   * @return contraints to add in JPanel
   */
  public GridBagConstraints constraintsFoodList() {
    GridBagConstraints constraints = new GridBagConstraints();
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.gridwidth = 0;
    constraints.weightx = 1;
    constraints.weighty = 1;
    return constraints;
  }

  /**
   * Create title Food up to foodRow displayed
   * 
   * @param title title of the food type
   * @return JLabel with food title name
   */
  public JLabel titleFood(String title) {
    JLabel label = new JLabel(title);
    label.setHorizontalAlignment(SwingConstants.CENTER);
    return label;

  }

  //
  // GETTERS
  //

  public OneFoodRow getDishRow() {
    return dishRow;
  }

  public OneFoodRow getDessertRow() {
    return dessertRow;
  }

  public OneFoodRow getBerverageRow() {
    return beverageRow;
  }

}
