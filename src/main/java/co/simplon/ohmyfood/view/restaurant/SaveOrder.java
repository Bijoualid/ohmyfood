package co.simplon.ohmyfood.view.restaurant;

import java.awt.BorderLayout;
import java.awt.Color;


import java.time.LocalTime;
import javax.annotation.PostConstruct;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import co.simplon.ohmyfood.controller.RestaurantController;
import co.simplon.ohmyfood.entity.Beverage;
import co.simplon.ohmyfood.entity.Dessert;
import co.simplon.ohmyfood.entity.Dish;
import co.simplon.ohmyfood.entity.OrderTable;
import co.simplon.ohmyfood.exception.NoFoodInOrderException;
import co.simplon.ohmyfood.exception.NotEnoughtStockOfFoodException;


@Component
public class SaveOrder extends JPanel {

  @Autowired
  private RestaurantController restaurantController;

  @Autowired
  private RightSideUp rightSideUp;

  @Autowired
  private AllFoodRows allFoodRows;

  @Autowired
  @Lazy
  private RightSide rightSide;

  private JLabel errorLabel;


  /**
   * metode to add a button that save the order in database Make a new order with table, waiter and
   * foods selected remove the foddlistOrder displayed to make a new order
   * 
   * @return saveOrder button
   */
  @PostConstruct
  public void saveOrder() {

    JButton saveOrderButton = new JButton("                Valider la commande                ");

    errorLabel = new JLabel();
    setLayout(new BorderLayout());
    add(saveOrderButton, BorderLayout.NORTH);


    saveOrderButton.addActionListener((e) -> saveButtonAction());
  }

  public void saveButtonAction() {
    remove(errorLabel);
    revalidate();
    repaint();
    OrderTable orderTable = new OrderTable(rightSideUp.getTableSelected(), LocalTime.now());
    orderTable.setWaiter(rightSideUp.getWaiterSelected());
    addFoodToOrder(orderTable);


    try {
      restaurantController.SaveOrder(orderTable);

    } catch (NoFoodInOrderException e1) {
      addErrorLable(e1);
      e1.printStackTrace();

    } catch (NotEnoughtStockOfFoodException e1) {
      addErrorLable(e1);
      e1.printStackTrace();
    }
    resetFoodList();
  }

  public void addErrorLable(Exception e1) {
    errorLabel.setText(e1.getMessage());
    errorLabel.setHorizontalAlignment(SwingConstants.CENTER);
    errorLabel.setForeground(Color.RED);
    add(errorLabel, BorderLayout.SOUTH);
  }

  /**
   * convert Food list to children list and add it to orderTable
   * 
   * @param orderTable ordertable to save list of desserts, dishes and beverages
   */
  public void addFoodToOrder(OrderTable orderTable) {


    allFoodRows.getBerverageRow().getFoodToOrder()
        .forEach(el -> orderTable.getBeverages().add((Beverage) el));

    allFoodRows.getDishRow().getFoodToOrder().forEach(el -> orderTable.getDishes().add((Dish) el));

    allFoodRows.getDessertRow().getFoodToOrder()
        .forEach(el -> orderTable.getDesserts().add((Dessert) el));

  }

  /**
   * reset the list of food added into right side
   */
  public void resetFoodList() {
    allFoodRows.getBerverageRow().getFoodToOrder().clear();
    allFoodRows.getDishRow().getFoodToOrder().clear();
    allFoodRows.getDessertRow().getFoodToOrder().clear();


    rightSide.remove(rightSide.getCenterPanel());
    rightSide.rightSideCenter();
    rightSide.add(rightSide.getCenterPanel(), 0);
  }



}
