package co.simplon.ohmyfood.view.restaurant;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JPanel;
import org.springframework.stereotype.Component;
import co.simplon.ohmyfood.entity.Food;


/**
 * Jlable display one food row can be used with all Food class children
 */
@Component
public class OneFoodRow extends JPanel {

  private List<? extends Food> databaseFoodList;
  private List<Food> foodListToOrder;



  /**
   * init JPanel
   * 
   * @param databaseFoodList list of Food to display
   */
  public OneFoodRow(List<? extends Food> databaseFoodList) {

    foodListToOrder = new ArrayList<>();
    this.databaseFoodList = databaseFoodList;

    setBackground(Color.ORANGE); // add background color
    setLayout(new GridLayout()); // take layout to gridLayout
    loopOnFood();
  }


  /**
   * create list of button On click add food to list for save to database
   */
  public void loopOnFood() {

    removeAll(); // removeAll button for refreshing page

    for (Food food : databaseFoodList) {
      String buttonText =
          "<html> <div style= 'text-align: center'> <p> " + food.getName() + "</p> <p>"
              + food.getPrice() + " euro </p> <p> QTY : " + food.getStock() + " </div> </html>";

      JButton button = new JButton(buttonText);
      button.addActionListener((e) -> foodButtonAction(food));
      add(button);
    }
  }

  /**
   * On click, add food to list foodListToOrder. if foodListToOrder contain food, duplicate food
   * object in order to compare them.
   */
  public void foodButtonAction(Food food) {
    boolean foodIsInOrder = false;

    for (Food foodToOrder : foodListToOrder) {
      if (foodToOrder.getName().equals(food.getName())) {
        foodListToOrder.add(foodToOrder);
        foodIsInOrder = true;
        break;
      }
    }
    if (!foodIsInOrder) {
      foodListToOrder.add(food);
    }

  }



  //
  // GETTER AND SETTERS
  //

  public List<Food> getFoodToOrder() {
    return foodListToOrder;
  }


  public List<? extends Food> getDatabaseFoodList() {
    return databaseFoodList;
  }


  public void setDatabaseFoodList(List<? extends Food> databaseFoodList) {
    this.databaseFoodList = databaseFoodList;
  }



}
