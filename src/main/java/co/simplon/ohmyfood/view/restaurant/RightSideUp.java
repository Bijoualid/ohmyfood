package co.simplon.ohmyfood.view.restaurant;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import co.simplon.ohmyfood.controller.RestaurantController;
import co.simplon.ohmyfood.entity.Waiter;
import co.simplon.ohmyfood.exception.NoWaitersInDatabaseException;

@Component
public class RightSideUp extends JPanel {

  @Autowired
  private RestaurantController restaurantController;


  private int tableSelected = 1;
  private Waiter waiterSelected;
  private List<Waiter> waiters;

  private JComboBox<String> comboBoxTableChoice, comboBoxWaiterChoice;
  private Border borderBlue = BorderFactory.createLineBorder(Color.blue);
  private JLabel date, errorMessage;

  /**
   * Up part of rightSIde panel, contain hour, comboBox table choice, waiter comboBOx choice
   * 
   * @return up Part JPanel block
   */
  @PostConstruct
  public void init() {
    // init attributs
    waiterSelected = new Waiter();
    waiters = new ArrayList<>();
    errorMessage = new JLabel();


    setLayout(new GridLayout(4, 1));
    add(date());

    // init and add comboboxes
    comboBoxTableChoice = new JComboBox<>(tableMaker(10));
    comboBoxWaiterChoice = new JComboBox<>(waiterNameArray());

    JPanel PanelTableChoice = comboBoxMaker("table", comboBoxTableChoice);
    JPanel PanelWaiterChoice = comboBoxMaker("Serveur", comboBoxWaiterChoice);

    add(PanelTableChoice);
    add(PanelWaiterChoice);

    // listen to combo boxes
    comboBoxTableListener();
    comboBoxWaiterListener();

  }

  /**
   * methode to display the date with custom pattern and center it in the JPanel
   * 
   * @return
   */
  public JLabel date() {
    date = new JLabel("<html><div style= 'text-align: center'><p>"
        + LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")) + "</p> <p> "
        + LocalDate.now()
            .format(DateTimeFormatter.ofPattern("EEEE dd MMMM yyyy ", new Locale("fr", "FR")))
        + "</p> </div> </html>");// display date like 'jeudi 3 mars 2022'

    date.setBorder(borderBlue);
    date.setHorizontalAlignment(JLabel.CENTER);

    return date;
  }

  /**
   * Listener methode to change the value of the selected table when user change ComboBox value
   */
  public void comboBoxTableListener() {
    comboBoxTableChoice
        .addActionListener((e) -> tableSelected = comboBoxTableChoice.getSelectedIndex() + 1);
  }

  /**
   * Listener methode to change the value of the selected waiter when user change ComboBox value
   */
  public void comboBoxWaiterListener() {
    comboBoxWaiterChoice.addActionListener((e) -> {
      String waiterLastName = comboBoxWaiterChoice.getSelectedItem().toString();
      waiters
          .forEach(el -> waiterSelected = el.getLastName() == waiterLastName ? el : waiterSelected);

    });

  }



  /**
   * metode global to make a JPanel with a comboboxand his text title
   * 
   * @param title title of the comboBox
   * @param comboBox comboBoxx to display
   * @return JPanel with title and ComboBOx
   */
  public JPanel comboBoxMaker(String title, JComboBox<String> comboBox) {
    JPanel selectArea = new JPanel();
    JLabel comboBoxTitle = new JLabel(title);
    selectArea.add(comboBoxTitle, BorderLayout.WEST);
    selectArea.add(comboBox, BorderLayout.EAST);
    selectArea.setBorder(borderBlue);
    return selectArea;
  }

  public String[] tableMaker(int tableNumber) {
    String[] tables = new String[tableNumber];
    for (int i = 0; i < tableNumber; i++) {
      tables[i] = String.valueOf(i + 1);
    }
    return tables;
  }

  /**
   * methode to create a string array of waiters LastName to inject into ComboBox
   * 
   * @return waiterName string Array
   */
  public String[] waiterNameArray() {
    remove(errorMessage);
    try {
      waiters = restaurantController.findAllWaiters();
      waiterSelected = waiters.get(0);
      return waiters.stream().map(el -> el.getLastName()).toArray(String[]::new);


    } catch (NoWaitersInDatabaseException e) {
      errorMessage.setText(e.getMessage());
      errorMessage.setForeground(Color.RED);
      add(errorMessage);
      e.printStackTrace();
    }
    return new String[0];

  }

  //
  // GETTERS
  //

  public JLabel getDate() {
    return date;
  }

  public int getTableSelected() {
    return tableSelected;
  }

  public Waiter getWaiterSelected() {
    return waiterSelected;
  }

}
