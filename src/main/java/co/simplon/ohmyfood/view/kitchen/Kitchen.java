package co.simplon.ohmyfood.view.kitchen;

import java.awt.BorderLayout;
import javax.annotation.PostConstruct;
import javax.swing.JPanel;
import javax.swing.border.Border;

import java.awt.*;
import javax.swing.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import co.simplon.ohmyfood.view.restaurant.Header;

@Component
public class Kitchen extends JPanel {

  @Autowired
  private AllOrderRows allOrderRows;

  JPanel centerPage;

  // borders
  Border bo1 = BorderFactory.createLineBorder(Color.black);
  Border redColor = BorderFactory.createLineBorder(Color.RED);
  Border blueColor = BorderFactory.createLineBorder(Color.black);
  // Layout

  GridLayout gl = new GridLayout(5, 5);

  // this method will run the page kitchen
  @PostConstruct
  public void Init() {

    setLayout(new BorderLayout());
    centerPage = new JPanel(new BorderLayout());
    add(new Header(), BorderLayout.NORTH);
    centerPage.add(allOrderRows, BorderLayout.NORTH);
    add(centerPage, BorderLayout.CENTER);
    add(refreshButton(), BorderLayout.SOUTH);

  }

  public JButton refreshButton() {
    JButton refreshButton = new JButton("Rafraichir");
    refreshButton.addActionListener((e) -> refreshButtonAction());
    return refreshButton;
  }

  public void refreshButtonAction() {

    allOrderRows.removeAll();
    allOrderRows.init();
    allOrderRows.revalidate();
    allOrderRows.repaint();
  }



}
