package co.simplon.ohmyfood.view.kitchen;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.simplon.ohmyfood.controller.KitchenController;
import co.simplon.ohmyfood.entity.OrderTable;

@Component
public class AllOrderRows extends JPanel {


  private JPanel oneOrder;

  @Autowired
  private KitchenController kitchenController;


  @PostConstruct
  public void init() {

    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    loopOnOrders();


  }



  public void loopOnOrders() {
    List<OrderTable> orders = kitchenController.getOrders();
    for (OrderTable order : orders) {
      add(displayAllFoodList(order));
    }
  }



  /**
   * Display all rows in restaurant page in gridLayout Jpanel
   * 
   * @param kitchenController controller autowired in main window
   * @return Jpanel with all foods row
   */

  public JPanel displayAllFoodList(OrderTable order) {

    oneOrder = new JPanel(new GridLayout());

    // oneOrder.setLayout(new BoxLayout(oneOrder, BoxLayout.X_AXIS));
    OneOrderRow dishRow = new OneOrderRow(order.getDishes());
    OneOrderRow dessertRow = new OneOrderRow(order.getDesserts());
    OneOrderRow beverageRow = new OneOrderRow(order.getBeverages());

    oneOrder.add(displayOneType("numéro de commande", order.getId().toString()));
    oneOrder.add(displayOneType("Plats", dishRow));
    oneOrder.add(displayOneType("Dessert", dessertRow));
    oneOrder.add(displayOneType("Boissons", beverageRow));
    oneOrder.add(displayOneType("Serveur", order.getWaiter().getFirstName()));
    oneOrder.add(displayOneType("Table", order.getDinningTable().toString()));
    oneOrder.add(displayOneType("Heure de commande", order.getTime().toString()));
    oneOrder.add(deleteButton(order));

    return oneOrder;
  }

  public JButton deleteButton(OrderTable order) {
    JButton deleteButton = new JButton("Commande envoyée !");
    deleteButton.addActionListener((e) -> deleteButtonAction(order));
    return deleteButton;
  }

  public void deleteButtonAction(OrderTable order) {
    kitchenController.deleteOrder(order);
    removeAll();
    init();
    revalidate();
    repaint();
  }

  public JPanel displayOneType(String titre, OneOrderRow row) {
    JPanel oneType = new JPanel(new BorderLayout());
    oneType.add(titleFood(titre), BorderLayout.NORTH);
    oneType.add(row, BorderLayout.SOUTH);
    oneType.setBorder(BorderFactory.createLineBorder(Color.black));
    return oneType;
  }

  public JPanel displayOneType(String titre, String text) {
    JPanel oneType = new JPanel(new BorderLayout());
    oneType.add(titleFood(titre), BorderLayout.NORTH);
    oneType.add(titleFood(text), BorderLayout.SOUTH);
    oneType.setBorder(BorderFactory.createLineBorder(Color.black));
    return oneType;
  }



  /**
   * Create title Food up to foodRow displayed
   * 
   * @param title title of the food type
   * @return JLabel with food title name
   */
  public JLabel titleFood(String title) {
    JLabel label = new JLabel(title);
    label.setHorizontalAlignment(SwingConstants.CENTER);
    return label;

  }

}
