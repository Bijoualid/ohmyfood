package co.simplon.ohmyfood.view.kitchen;



import java.util.List;
import java.util.Map;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import org.springframework.stereotype.Component;


import co.simplon.ohmyfood.entity.Food;
import co.simplon.ohmyfood.view.SharedServices;

@Component
public class OneOrderRow extends JPanel {

  private List<? extends Food> databaseOrderList;



  /**
   * init JPanel
   * 
   * @param databaseOrderList list of Food to display
   */
  public OneOrderRow(List<? extends Food> databaseOrderList) {

    this.databaseOrderList = databaseOrderList;
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    loopOnFood();

  }

  /**
   * create list of button On click add food to list for save to database
   */
  public void loopOnFood() {
    if (databaseOrderList.isEmpty()) {
      JLabel text =
          new JLabel("<html> <div style= 'text-align: center'> <p> aucun </p></div> </html>");
      text.setHorizontalAlignment(SwingConstants.CENTER);
      add(text);
    } else {
      Map<? extends Food, Long> foodMap = SharedServices.convertListToHashMap(databaseOrderList);

      foodMap.forEach((food, quantity) -> {
        String buttonText = "<html> <div style= 'text-align: center'> <p> " + quantity + "x "
            + food.getName() + "</p></div> </html>";
        JLabel text = new JLabel(buttonText);
        text.setHorizontalAlignment(SwingConstants.CENTER);
        add(text);
      });
    }


  }



  //
  // GETTER AND SETTERS
  //


  public List<? extends Food> getDatabaseOrderList() {
    return databaseOrderList;
  }


  public void setDatabaseOrderList(List<? extends Food> databaseOrderList) {
    this.databaseOrderList = databaseOrderList;
  }


}
