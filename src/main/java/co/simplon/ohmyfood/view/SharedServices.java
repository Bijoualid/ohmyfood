package co.simplon.ohmyfood.view;


import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import co.simplon.ohmyfood.entity.Food;

@Service
public class SharedServices {


  /**
   * convert list of food to hashMap with recurrency in value
   * 
   * @param foodList list of food to convert
   * @return hashmap
   */
  public static Map<? extends Food, Long> convertListToHashMap(List<? extends Food> foodList) {

    return foodList.stream()
        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
  }
}
