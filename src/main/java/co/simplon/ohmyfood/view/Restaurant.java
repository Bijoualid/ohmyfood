package co.simplon.ohmyfood.view;

import java.awt.BorderLayout;
import javax.annotation.PostConstruct;
import javax.swing.JPanel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import co.simplon.ohmyfood.view.restaurant.AllFoodRows;
import co.simplon.ohmyfood.view.restaurant.Header;
import co.simplon.ohmyfood.view.restaurant.RightSide;

/**
 * Restaurant page Jpanel
 */
@Component
public class Restaurant extends JPanel {


  @Autowired
  private Header header;

  @Autowired
  @Lazy
  private Window window;

  @Autowired
  private RightSide rightSide;

  @Autowired
  private AllFoodRows allFoodRows;


  /**
   * Init the restaurant page. init attributs + add layouts main blocks of restaurant page launch
   * comboBox listeners launch refreshPage for hour
   */
  @PostConstruct
  public void Init() {

    setLayout(new BorderLayout());
    add(header, BorderLayout.NORTH);
    add(allFoodRows, BorderLayout.CENTER);
    add(rightSide, BorderLayout.EAST);



  }



  // TODO ajouter bouton retour page accueil
  // TODO récupérer la list enregister dans la database par table ???



}
