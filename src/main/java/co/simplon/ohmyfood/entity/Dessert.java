package co.simplon.ohmyfood.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;


/**
 * dessert food entity mapped with many to many to the orders. extends Food abstract class
 */
@Entity
public class Dessert extends Food {

  @ManyToMany(mappedBy = "desserts")
  private List<OrderTable> orders = new ArrayList<>();

  public Dessert(String name, int price, int stock) {
    super(name, price, stock);
  }

  public Dessert() {}

  public Dessert(int id, String name, int price, int stock) {
    super(id, name, price, stock);
  }

  public List<OrderTable> getOrders() {
    return orders;
  }

  public void setOrders(List<OrderTable> orders) {
    this.orders = orders;
  }

}
