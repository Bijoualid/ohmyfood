package co.simplon.ohmyfood.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;


/**
 * beverage food entity mapped with many to many to the orders. extends Food abstract class
 */
@Entity
public class Beverage extends Food {

  @ManyToMany(mappedBy = "beverages")
  private List<OrderTable> orders = new ArrayList<>();

  //
  // Constructors
  //

  public Beverage(String name, int price, int stock) {
    super(name, price, stock);
  }

  public Beverage() {}

  public Beverage(int id, String name, int price, int stock) {
    super(id, name, price, stock);
  }

  //
  // getters an setters
  //
  public List<OrderTable> getOrders() {
    return orders;
  }

  public void setOrders(List<OrderTable> orders) {
    this.orders = orders;
  }
}
