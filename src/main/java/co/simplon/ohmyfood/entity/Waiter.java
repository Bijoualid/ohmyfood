package co.simplon.ohmyfood.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * entity of waiter that make the order mapped with one to many to the orders
 */
@Entity
public class Waiter implements Comparable<Waiter> {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  private String firstName;
  private String lastName;

  @OneToMany(mappedBy = "waiter")
  private List<OrderTable> orders = new ArrayList<>();

  //
  // constructors
  //
  public Waiter(String firstName, String lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  public Waiter(Integer id, String firstName, String lastName) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
  }

  public Waiter() {}

  // getters and setters
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public List<OrderTable> getOrders() {
    return orders;
  }

  public void setOrders(List<OrderTable> orders) {
    this.orders = orders;
  }

  @Override
  public int compareTo(Waiter waiter) {
    return (this.lastName.compareTo(waiter.lastName));
  }

}
