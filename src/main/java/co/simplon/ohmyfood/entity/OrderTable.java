package co.simplon.ohmyfood.entity;


import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * This class contains the order with the table number and order time store list of Beverage,
 * Dessert, Dish to send back to kitchen
 */
@Entity
@Table(name = "order_table")
public class OrderTable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  private Integer dinningTable;
  private LocalTime time;

  @ManyToOne
  private Waiter waiter;

  @ManyToMany(fetch = FetchType.EAGER)
  @Fetch(value = FetchMode.SUBSELECT)
  @JoinTable(name = "beverage_orders")

  private List<Beverage> beverages = new ArrayList<>();

  @ManyToMany(fetch = FetchType.EAGER)
  @Fetch(value = FetchMode.SUBSELECT)
  @JoinTable(name = "dessert_orders")
  private List<Dessert> desserts = new ArrayList<>();

  @ManyToMany(fetch = FetchType.EAGER)
  @Fetch(value = FetchMode.SUBSELECT)
  @JoinTable(name = "dish_orders")
  private List<Dish> dishes = new ArrayList<>();

  //
  // constructors
  //

  public OrderTable(Integer table, LocalTime time) {
    this.dinningTable = table;
    this.time = time;
  }

  public OrderTable(Integer id, Integer table, LocalTime time) {
    this.id = id;
    this.dinningTable = table;
    this.time = time;
  }

  public OrderTable() {}

  //
  // Getters and Setters
  //

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }


  public LocalTime getTime() {
    return time;
  }

  public void setTime(LocalTime time) {
    this.time = time;

  }

  public Integer getDinningTable() {
    return dinningTable;
  }

  public void setDinningTable(Integer dinningTable) {
    this.dinningTable = dinningTable;
  }

  public Waiter getWaiter() {
    return waiter;
  }

  public void setWaiter(Waiter waiter) {
    this.waiter = waiter;
  }

  public List<Beverage> getBeverages() {
    return beverages;
  }

  public void setBeverages(List<Beverage> beverages) {
    this.beverages = beverages;
  }

  public List<Dessert> getDesserts() {
    return desserts;
  }

  public void setDesserts(List<Dessert> desserts) {
    this.desserts = desserts;
  }

  public List<Dish> getDishes() {
    return dishes;
  }

  public void setDishes(List<Dish> dishes) {
    this.dishes = dishes;
  }
}
