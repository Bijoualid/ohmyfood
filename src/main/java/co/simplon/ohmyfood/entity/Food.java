package co.simplon.ohmyfood.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.MappedSuperclass;

/**
 * main class of food elements. MappedSuperClass tell JPA don't create Food table in database
 */
@MappedSuperclass
public abstract class Food {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;
  private String name;
  private int price;
  private int stock;


  public Food(String name, int price, int stock) {
    this.name = name;
    this.price = price;
    this.stock = stock;

  }

  public Food() {}

  public Food(int id, String name, int price, int stock) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.stock = stock;

  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public int getStock() {
    return stock;
  }

  public void setStock(int stock) {
    this.stock = stock;
  }

  public void decreaseStock() {
    this.stock--;
  }


}
