INSERT INTO
  `waiter` (first_name, last_name)
VALUES
  ('Roland', 'Foucher');
INSERT INTO
  `waiter` (first_name, last_name)
VALUES
  ('Khaled', 'Razouk');
INSERT INTO
  `beverage` (name, price, stock)
VALUES
  ('Fanta', 3, 10);
INSERT INTO
  `beverage` (name, price, stock)
VALUES
  ('Oasis', 4, 6);
INSERT INTO
  `dessert` (name, price, stock)
VALUES
  ('Tarte au citron', 5, 27);
INSERT INTO
  `dessert` (name, price, stock)
VALUES
  ('Tiramisu', 6, 18);
INSERT INTO
  `dish` (name, price, stock)
VALUES
  ('Ravioles de foie gras', 18, 9);
INSERT INTO
  `dish` (name, price, stock)
VALUES
  ('Foie gras de canard', 21, 15);
INSERT INTO
  order_table (dinning_table, time, waiter_id)
VALUES
  (1, '10:10:00.000000', 1);
INSERT INTO
  order_table (dinning_table, time, waiter_id)
VALUES
  (2, '10:11:00.000000', 2);
INSERT INTO
  beverage_orders
VALUES
  (1, 1);
INSERT INTO
  beverage_orders
VALUES
  (2, 2);
INSERT INTO
  dessert_orders
VALUES
  (1, 1);
INSERT INTO
  dessert_orders
VALUES
  (2, 2);
INSERT INTO
  dish_orders
VALUES
  (1, 1);
INSERT INTO
  dish_orders
VALUES
  (2, 2);